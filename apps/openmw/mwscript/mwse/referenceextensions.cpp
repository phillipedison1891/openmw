#include "referenceextensions.hpp"

#include <components/compiler/extensions.hpp>
#include <components/compiler/opcodes.hpp>
#include <components/compiler/mwse/opcodes.hpp>

#include <components/interpreter/interpreter.hpp>
#include <components/interpreter/runtime.hpp>
#include <components/interpreter/opcodes.hpp>
#include <components/interpreter/context.hpp>

#include <components/esm/loadench.hpp>
#include <components/esm/loadweap.hpp>
#include <boost/algorithm/string.hpp>

#include <cmath>
#include <typeinfo>

#include "stringextensions.hpp"
#include "../ref.hpp"
#include "../../mwworld/class.hpp"
#include "../../mwworld/esmstore.hpp"
#include "../../mwbase/environment.hpp"
#include "../../mwbase/world.hpp"
#include "../../mwworld/cellstore.hpp"
#include "../interpretercontext.hpp"

namespace MWScript
{
    namespace MWSE
    {
        namespace Reference
        {

            RefType getMWSERefType(MWWorld::Ptr &ptr)
            {
                const std::string typeName = ptr.getTypeName();
                RefType result;

                if (typeName == typeid(ESM::Activator).name())
                    result = RefType::Activator;
                else if (typeName == typeid(ESM::Potion).name())
                    result = RefType::Alchemy;
                else if (typeName == typeid(ESM::Apparatus).name())
                    result = RefType::Apparatus;
                else if (typeName == typeid(ESM::Armor).name())
                    result = RefType::Armor;
                else if (typeName == typeid(ESM::Book).name())
                    result = RefType::Book;
                else if (typeName == typeid(ESM::Clothing).name())
                    result = RefType::Clothing;
                else if (typeName == typeid(ESM::Container).name())
                    result = RefType::Container;
                else if (typeName == typeid(ESM::Creature).name())
                    result = RefType::Creature;
                else if (typeName == typeid(ESM::Door).name())
                    result = RefType::Door;
                else if (typeName == typeid(ESM::Ingredient).name())
                    result = RefType::Ingredient;
                else if (typeName == typeid(ESM::CreatureLevList).name())
                    result = RefType::LevelledCreature;
                else if (typeName == typeid(ESM::ItemLevList).name())
                    result = RefType::LevelledItem;
                else if (typeName == typeid(ESM::Light).name())
                    result = RefType::Light;
                else if (typeName == typeid(ESM::Lockpick).name())
                    result = RefType::Lockpick;
                else if (typeName == typeid(ESM::Miscellaneous).name())
                    result = RefType::Misc;
                else if (typeName == typeid(ESM::NPC).name())
                    result = RefType::NPC;
                else if (typeName == typeid(ESM::Probe).name())
                    result = RefType::Probe;
                else if (typeName == typeid(ESM::Repair).name())
                    result = RefType::Repair;
                else if (typeName == typeid(ESM::Script).name())
                    result = RefType::Script;
                else if (typeName == typeid(ESM::SoundGenerator).name())
                    result = RefType::SoundGenerator;
                else if (typeName == typeid(ESM::Spell).name())
                    result = RefType::Spell;
                else if (typeName == typeid(ESM::Static).name())
                    result = RefType::Static;
                else if (typeName == typeid(ESM::Weapon).name())
                    if (ptr.get<ESM::Weapon>()->mBase->mData.mType == ESM::Weapon::Arrow
                        || ptr.get<ESM::Weapon>()->mBase->mData.mType == ESM::Weapon::Bolt)
                        result = RefType::Ammunition;
                    else
                        result = RefType::Weapon;
                else
                    result = RefType::Unknown;
                return result;

            }

            template<class R>
            class OpXGetName : public Interpreter::Opcode0
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime)
                {
                    MWWorld::Ptr ptr = R()(runtime);
                    std::string name = ptr.getClass().getName(ptr);
                    runtime.push(static_cast<MWScript::InterpreterContext *>(&(runtime.getContext()))->getMWSEContext().addStringVariable(name));
                    
                }
            };

            template<class R>
            class OpXGetBaseId : public Interpreter::Opcode0
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime)
                {
                    MWWorld::Ptr ptr = R()(runtime);
                    std::string objId = ptr.getCellRef().getRefId();
                    runtime.push(static_cast<MWScript::InterpreterContext *>(&(runtime.getContext()))->getMWSEContext().addStringVariable(objId));
                }

            };

            template<class R>
            class OpXGetCharge : public Interpreter::Opcode0
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime)
                {
                    MWWorld::Ptr ptr = R()(runtime);
                    Interpreter::Type_Float charge = ptr.getCellRef().getEnchantmentCharge();
                    if (ptr.getClass().getEnchantment(ptr) == "")
                        charge = 0;
                    else if (charge == -1)
                    {
                        const ESM::Enchantment *ench = MWBase::Environment::get().getWorld()->getStore().get<ESM::Enchantment>().find(
                            ptr.getClass().getEnchantment(ptr));
                        charge = ench->mData.mCharge;
                    }

                    runtime.push(charge);
                }
            };

            template<class R>
            class OpXGetCondition : public Interpreter::Opcode0
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime)
                {
                    MWWorld::Ptr ptr = R()(runtime);
                    Interpreter::Type_Float condition = ptr.getCellRef().getCharge();
                    if (condition == -1)
                    {
                        if (ptr.getTypeName() == typeid(ESM::Armor).name())
                        {
                            const ESM::Armor *armor = ptr.get<ESM::Armor>()->mBase;
                            condition = armor->mData.mHealth;
                        }
                        else if (ptr.getTypeName() == typeid(ESM::Weapon).name())
                        {
                            const ESM::Weapon *weapon = ptr.get<ESM::Weapon>()->mBase;
                            condition = weapon->mData.mHealth;
                        }
                        else if (ptr.getTypeName() == typeid(ESM::Lockpick).name())
                        {
                            const ESM::Lockpick *lockpick = ptr.get<ESM::Lockpick>()->mBase;
                            condition = lockpick->mData.mUses;
                        }
                        else if (ptr.getTypeName() == typeid(ESM::Probe).name())
                        {
                            const ESM::Probe *probe = ptr.get<ESM::Probe>()->mBase;
                            condition = probe->mData.mUses;
                        }
                        else if (ptr.getTypeName() == typeid(ESM::Repair).name())
                        {
                            const ESM::Repair *repair = ptr.get<ESM::Repair>()->mBase;
                            condition = repair->mData.mUses;
                        }
                    }
                    runtime.push(condition);
                }
            };

            template<class R>
            class OpXGetEncumb : public Interpreter::Opcode0
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime)
                {
                    MWWorld::Ptr ptr = R()(runtime);
                    Interpreter::Type_Float encumb = ptr.getClass().getEncumbrance(ptr);
                    runtime.push(encumb);
                }
            };

            template<class R>
            class OpXGetLockLevel : public Interpreter::Opcode0
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime)
                {
                    MWWorld::Ptr ptr = R()(runtime);
                    Interpreter::Type_Integer level = std::abs(ptr.getCellRef().getLockLevel());
                    if (level == 0) level = -1;
                    runtime.push(level);
                }
            };

            template<class R>
            class OpXGetMaxCharge : public Interpreter::Opcode0
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime)
                {
                    MWWorld::Ptr ptr = R()(runtime);
                    const ESM::Enchantment *ench = MWBase::Environment::get().getWorld()->getStore().get<ESM::Enchantment>().find(
                        ptr.getClass().getEnchantment(ptr));
                    runtime.push(ench->mData.mCharge);
                }
            };

            template<class R>
            class OpXGetMaxCondition : public Interpreter::Opcode0
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime)
                {
                    MWWorld::Ptr ptr = R()(runtime);
                    Interpreter::Type_Float condition;
                    if (ptr.getTypeName() == typeid(ESM::Armor).name())
                    {
                        const ESM::Armor *armor = ptr.get<ESM::Armor>()->mBase;
                        condition = armor->mData.mHealth;
                    }
                    else if (ptr.getTypeName() == typeid(ESM::Weapon).name())
                    {
                        const ESM::Weapon *weapon = ptr.get<ESM::Weapon>()->mBase;
                        condition = weapon->mData.mHealth;
                    }
                    else if (ptr.getTypeName() == typeid(ESM::Lockpick).name())
                    {
                        const ESM::Lockpick *lockpick = ptr.get<ESM::Lockpick>()->mBase;
                        condition = lockpick->mData.mUses;
                    }
                    else if (ptr.getTypeName() == typeid(ESM::Probe).name())
                    {
                        const ESM::Probe *probe = ptr.get<ESM::Probe>()->mBase;
                        condition = probe->mData.mUses;
                    }
                    else if (ptr.getTypeName() == typeid(ESM::Repair).name())
                    {
                        const ESM::Repair *repair = ptr.get<ESM::Repair>()->mBase;
                        condition = repair->mData.mUses;
                    }
                    else
                    {
                        condition = 0.0;
                    }
                    runtime.push(condition);
                }
            };

            template<class R>
            class OpXGetQuality : public Interpreter::Opcode0
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime)
                {
                    MWWorld::Ptr ptr = R()(runtime);
                    Interpreter::Type_Float quality;
                    if (ptr.getTypeName() == typeid(ESM::Apparatus).name())
                    {
                        quality = ptr.get<ESM::Apparatus>()->mBase->mData.mQuality;
                    }
                    else if (ptr.getTypeName() == typeid(ESM::Lockpick).name())
                    {
                        quality = ptr.get<ESM::Lockpick>()->mBase->mData.mQuality;
                    }
                    else if (ptr.getTypeName() == typeid(ESM::Probe).name())
                    {
                        quality = ptr.get<ESM::Probe>()->mBase->mData.mQuality;
                    }
                    else if (ptr.getTypeName() == typeid(ESM::Repair).name())
                    {
                        quality = ptr.get<ESM::Repair>()->mBase->mData.mQuality;
                    }
                    else
                    {
                        quality = 0.0;
                    }
                    runtime.push(quality);
                }
            };

            template<class R>
            class OpXGetRef : public Interpreter::Opcode1
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime, unsigned int arg0)
                {
                    MWWorld::Ptr ptr = R()(runtime);
                    MWSEContext &con = (static_cast<MWScript::InterpreterContext *>(&(runtime.getContext())))->getMWSEContext();
                    if (arg0 == 0)
                    {
                        runtime.push(con.addReference(ptr));
                    }
                    else
                    {
                        Interpreter::Type_Integer strIndex = runtime[0].mInteger;
                        runtime.pop();
                        std::string id = String::getString(runtime, strIndex);
                        MWWorld::Ptr ref = MWBase::Environment::get().getWorld()->searchPtr(id, false);
                        if (ref.isEmpty())
                            runtime.push(0);
                        else
                            runtime.push(con.addReference(ref));
                    }
                }
            };

            template<class R>
            class OpXGetStackSize : public Interpreter::Opcode0
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime)
                {
                    MWWorld::Ptr ptr = R()(runtime);
                    if (boost::algorithm::to_lower_copy(ptr.getCellRef().getRefId()).find("gold_") == 0)
                        runtime.push(ptr.getCellRef().getGoldValue());
                    else
                        runtime.push(ptr.getRefData().getCount());
                }
            };

            template<class R>
            class OpXGetValue : public Interpreter::Opcode0
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime)
                {
                    MWWorld::Ptr ptr = R()(runtime);
                    runtime.push(ptr.getClass().getValue(ptr));
                }
            };

            template<class R>
            class OpXGetWeight : public Interpreter::Opcode0
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime)
                {
                    MWWorld::Ptr ptr = R()(runtime);
                    if (ptr.getTypeName() == typeid(ESM::Container).name())
                        runtime.push(ptr.get<ESM::Container>()->mBase->mWeight);
                    else
                        runtime.push(ptr.getClass().getWeight(ptr) * ptr.getRefData().getCount());
                }
            };

            template<class R>
            class OpXMyCellID : public Interpreter::Opcode0
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime)
                {
                    MWWorld::Ptr ptr = R()(runtime);
                    MWScript::InterpreterContext& context = static_cast<MWScript::InterpreterContext&> (runtime.getContext());
                    std::string cellid = ptr.getCell()->getCell()->getDescription();
                    runtime.push(context.getMWSEContext().addStringVariable(cellid));
                }
            };

            template<class R>
            class OpXMyRef : public Interpreter::Opcode0
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime)
                {
                    OpXGetRef<R>().execute(runtime, 0);

                }
            };

            template<class R>
            class OpXRefType : public Interpreter::Opcode0
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime)
                {
                    MWWorld::Ptr ptr = R()(runtime);
                    runtime.push(static_cast<Interpreter::Type_Integer>(getMWSERefType(ptr)));
                }
            };

            void installOpcodes(Interpreter::Interpreter& interpreter)
            {
                /*interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXAddItem, new OpXAddItem<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXAddItemExplicit, new OpXAddItem<ExplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXRemoveItem, new OpXAddItem<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXRemoveItemExplicit, new OpXAddItem<ExplicitRef>);*/
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetName, new OpXGetName<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetNameExplicit, new OpXGetName<ExplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetBaseId, new OpXGetBaseId<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetBaseIdExplicit, new OpXGetBaseId<ExplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetCharge, new OpXGetCharge<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetChargeExplicit, new OpXGetCharge<ExplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetCondition, new OpXGetCondition<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetConditionExplicit, new OpXGetCondition<ExplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetEncumb, new OpXGetEncumb<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetEncumbExplicit, new OpXGetEncumb<ExplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetLockLevel, new OpXGetLockLevel<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetLockLevelExplicit, new OpXGetLockLevel<ExplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetMaxCharge, new OpXGetMaxCharge<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetMaxChargeExplicit, new OpXGetMaxCharge<ExplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetMaxCondition, new OpXGetMaxCondition<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetMaxConditionExplicit, new OpXGetMaxCondition<ExplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetQuality, new OpXGetQuality<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetQualityExplicit, new OpXGetQuality<ExplicitRef>);
                interpreter.installSegment3(Compiler::MWSE::Reference::opcodeXGetRef, new OpXGetRef<ImplicitRef>);
                interpreter.installSegment3(Compiler::MWSE::Reference::opcodeXGetRefExplicit, new OpXGetRef<ExplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetStackSize, new OpXGetStackSize<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetStackSizeExplicit, new OpXGetStackSize<ExplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetValue, new OpXGetValue<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetValueExplicit, new OpXGetValue<ExplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetWeight, new OpXGetWeight<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXGetWeightExplicit, new OpXGetWeight<ExplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXMyCellId, new OpXMyCellID<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXMyCellIdExplicit, new OpXMyCellID<ExplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXMyRef, new OpXMyRef<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXMyRefExplicit, new OpXMyRef<ExplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXRefType, new OpXRefType<ImplicitRef>);
                interpreter.installSegment5(Compiler::MWSE::Reference::opcodeXRefTypeExplicit, new OpXRefType<ExplicitRef>);
            }
        }
    }
}