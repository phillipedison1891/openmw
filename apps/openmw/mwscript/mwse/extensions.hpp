#ifndef GAME_SCRIPT_MWSE_EXTENSIONS_H
#define GAME_SCRIPT_MWSE_EXTENSIONS_H

namespace Compiler
{
    class Extensions;
}

namespace Interpreter
{
    class Interpreter;
}

namespace MWScript
{
    namespace MWSE
    {
        void installOpcodes (Interpreter::Interpreter& interpreter, bool consoleOnly = false);
        ///< \param consoleOnly include console only opcodes
    }
}

#endif