#include "logicextensions.hpp"

#include <components/compiler/mwse/opcodes.hpp>
#include <components/compiler/locals.hpp>

#include <components/interpreter/interpreter.hpp>
#include <components/interpreter/runtime.hpp>
#include <components/interpreter/opcodes.hpp>

namespace MWScript
{
    namespace MWSE
    {
        namespace Logic
        {

            class OpXAnd : public Interpreter::Opcode0
            {
                public:
                    virtual void execute(Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Integer arg1 = runtime[0].mInteger;
                        runtime.pop();
                        Interpreter::Type_Integer arg2 = runtime[0].mInteger;
                        runtime.pop();

                        runtime.push(arg1 && arg2);
                    }
            };

            class OpXOr : public Interpreter::Opcode0
            {
                public:
                    virtual void execute(Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Integer arg1 = runtime[0].mInteger;
                        runtime.pop();
                        Interpreter::Type_Integer arg2 = runtime[0].mInteger;
                        runtime.pop();

                        runtime.push(arg1 || arg2);
                    }
            };

            class OpXXor : public Interpreter::Opcode0
            {
            public:
                virtual void execute(Interpreter::Runtime& runtime)
                {
                    Interpreter::Type_Integer arg1 = runtime[0].mInteger;
                    runtime.pop();
                    Interpreter::Type_Integer arg2 = runtime[0].mInteger;
                    runtime.pop();

                    runtime.push(((bool)arg1) != ((bool)arg2));
                }
            };

            class OpXNot : public Interpreter::Opcode0
            {
                public:
                    virtual void execute(Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Integer arg = runtime[0].mInteger;
                        runtime.pop();

                        runtime.push(!(bool)arg);
                    }
            };

            void installOpcodes(Interpreter::Interpreter& interpreter)
            {
                interpreter.installSegment5(Compiler::MWSE::Logic::opcodeXAnd, new OpXAnd);
                interpreter.installSegment5(Compiler::MWSE::Logic::opcodeXOr, new OpXOr);
                interpreter.installSegment5(Compiler::MWSE::Logic::opcodeXXor, new OpXXor);
                interpreter.installSegment5(Compiler::MWSE::Logic::opcodeXNot, new OpXNot);
            }
        }
    }
}