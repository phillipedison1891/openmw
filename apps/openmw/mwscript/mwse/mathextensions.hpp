#ifndef GAME_SCRIPT_MWSE_MATHEXTENSIONS_H
#define GAME_SCRIPT_MWSE_MATHEXTENSIONS_H

namespace Compiler
{
    class Extensions;
}

namespace Interpreter
{
    class Interpreter;
}

namespace MWScript
{
    namespace MWSE
    {
        namespace Math
        {
            void installOpcodes(Interpreter::Interpreter& interpreter);
        }
    }
}

#endif