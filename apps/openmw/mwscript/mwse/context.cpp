#include "context.hpp"

namespace MWScript
{
    namespace MWSE
    {
        Interpreter::Type_Integer MWSEContext::addStringVariable(std::string str)
        {
            stringVariables.insert(std::pair<Interpreter::Type_Integer, std::string>(nextStringIndex, str));
            return nextStringIndex--;
        }

        std::string& MWSEContext::getStringVariable(Interpreter::Type_Integer index)
        {
            if (stringVariables.count(index) != 1)
            {
                throw std::out_of_range("out of range");
            }
            else
            {
                return stringVariables[index];
            }
        }

        Interpreter::Type_Integer MWSEContext::addReference(MWWorld::Ptr ptr)
        {
            references.insert(std::pair<Interpreter::Type_Integer, MWWorld::Ptr>(nextRefIndex, ptr));
            return nextRefIndex--;
        }

        MWWorld::Ptr MWSEContext::getReference(Interpreter::Type_Integer index)
        {
            if (references.count(index) != 1)
            {
                throw std::out_of_range("out of range");
            }
            else
            {
                return references[index];
            }
        }


    }
}