#ifndef GAME_SCRIPT_MWSE_STRINGEXTENSIONS_H
#define GAME_SCRIPT_MWSE_STRINGEXTENSIONS_H

#include <string>
#include <vector>
#include <components/interpreter/interpreter.hpp>

namespace Compiler
{
    class Extensions;
}
namespace MWScript
{
    namespace MWSE
    {
        namespace String
        {
            std::string parseFormatString(std::string format, std::vector<Interpreter::Data> args);
            std::string getString(Interpreter::Runtime &runtime, int index);
            void installOpcodes(Interpreter::Interpreter& interpreter);
        }
    }
}


#endif