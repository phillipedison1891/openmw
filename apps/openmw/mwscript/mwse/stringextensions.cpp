#include "stringextensions.hpp"

#include <components/compiler/extensions.hpp>
#include <components/compiler/opcodes.hpp>
#include <components/compiler/mwse/opcodes.hpp>

#include <components/interpreter/interpreter.hpp>
#include <components/interpreter/runtime.hpp>
#include <components/interpreter/opcodes.hpp>
#include <components/interpreter/context.hpp>
#include <components/interpreter/miscopcodes.hpp>
#include <string>

#include "../interpretercontext.hpp"

namespace MWScript
{
    namespace MWSE
    {
        namespace String
        {

            std::string getString(Interpreter::Runtime &runtime, Interpreter::Type_Integer index) {
                std::string result;
                if (index >= 0)
                    result = runtime.getStringLiteral(index);
                else
                    result = (static_cast<MWScript::InterpreterContext *>(&(runtime.getContext())))->getMWSEContext().getStringVariable(index);
                return result;
            }

            // Will eventually be used for more robust format string handling
            std::string parseFormatString(std::string format, std::vector<Interpreter::Data> args)
            {
                return format;
            }

            class OpXStringBuild : public Interpreter::Opcode1
            {
                public:
                    virtual void execute(Interpreter::Runtime &runtime, unsigned int arg0)
                    {
                        std::string format = getString(runtime, runtime[0].mInteger);
                        runtime.pop();
                        // This should be eventually replaced by more robust format string handling
                        std::string result = Interpreter::formatMessage(format, runtime);
                        runtime.push(static_cast<MWScript::InterpreterContext *>(&(runtime.getContext()))->getMWSEContext().addStringVariable(result));
                    }
            };

            class OpXStringCompare : public Interpreter::Opcode0
            {
                public:
                    virtual void execute(Interpreter::Runtime &runtime)
                    {
                        std::string str1 = getString(runtime, runtime[0].mInteger);
                        runtime.pop();
                        std::string str2 = getString(runtime, runtime[0].mInteger);
                        runtime.pop();
                        runtime.push(str1.compare(str2));
                    }
            };

            class OpXStringLength : public Interpreter::Opcode0
            {
                public:
                    virtual void execute(Interpreter::Runtime &runtime)
                    {
                        std::string str = getString(runtime, runtime[0].mInteger);
                        runtime.pop();
                        runtime.push(static_cast<Interpreter::Type_Integer>(str.length()));
                    }
            };

            void installOpcodes(Interpreter::Interpreter & interpreter)
            {
                interpreter.installSegment3(Compiler::MWSE::String::opcodeXStringBuild, new OpXStringBuild);
                interpreter.installSegment5(Compiler::MWSE::String::opcodeXStringCompare, new OpXStringCompare);
                interpreter.installSegment5(Compiler::MWSE::String::opcodeXStringLength, new OpXStringLength);
            }
        }
    }
}
