#ifndef GAME_SCRIPT_MWSE_REFERENCEEXTENSIONS_H
#define GAME_SCRIPT_MWSE_REFERENCEEXTENSIONS_H
#include "../interpretercontext.hpp"

namespace Compiler
{
    class Extensions;
}

namespace Interpreter
{
    class Interpreter;
}

namespace MWScript
{
    namespace MWSE
    {
        namespace Reference
        {
            typedef enum struct RefType : Interpreter::Type_Integer
            {
                Activator = 1230259009,
                Alchemy = 1212369985,
                Ammunition = 1330466113,
                Apparatus = 1095782465,
                Armor = 1330467393,
                Book = 1263488834,
                Clothing = 1414483011,
                Container = 1414418243,
                Creature = 1095062083,
                Door = 1380929348,
                Ingredient = 1380404809,
                LevelledCreature = 1129727308,
                LevelledItem = 1230390604,
                Light = 1212631372,
                Lockpick = 1262702412,
                Misc = 1129531725,
                NPC = 1598246990,
                Probe = 1112494672,
                Repair = 1095779666,
                Script = 1414546259,
                SoundGenerator = 1195658835,
                Spell = 1279610963,
                Static = 1413567571,
                Weapon = 1346454871,
                Unknown = 0
            } RefType;

            RefType getMWSERefType(MWWorld::Ptr & ptr);
            void installOpcodes(Interpreter::Interpreter& interpreter);
        }
    }
}

#endif