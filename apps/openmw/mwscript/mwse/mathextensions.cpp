#include "mathextensions.hpp"

#include <components/compiler/mwse/opcodes.hpp>
#include <components/compiler/locals.hpp>

#include <components/interpreter/interpreter.hpp>
#include <components/interpreter/runtime.hpp>
#include <components/interpreter/opcodes.hpp>

#include <components/misc/rng.hpp>

#include <cmath>

namespace MWScript
{
    namespace MWSE
    {
        namespace Math
        {
            const double PI = 3.1415927;

            class OpXSqrt : public Interpreter::Opcode0
            {
                public:
                
                    virtual void execute (Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Float operand = runtime[0].mFloat;
                        runtime.pop();
                        
                        runtime.push(std::sqrt(operand));
                    }
            };

            class OpXSin : public Interpreter::Opcode0
            {
                public:

                    virtual void execute (Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Float operand = runtime[0].mFloat;
                        runtime.pop();

                        runtime.push(std::sin(operand));
                    }
            };

            class OpXCos : public Interpreter::Opcode0
            {
                public:

                    virtual void execute (Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Float operand = runtime[0].mFloat;
                        runtime.pop();

                        runtime.push(std::cos(operand));
                    }
            };

            class OpXTan : public Interpreter::Opcode0
            {
                public:

                    virtual void execute (Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Float operand = runtime[0].mFloat;
                        runtime.pop();

                        runtime.push(std::cos(operand));
                    }
            };
            
            class OpXArcSin : public Interpreter::Opcode0
            {
                public:
                    virtual void execute (Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Float operand = runtime[0].mFloat;
                        runtime.pop();

                        runtime.push(std::asin(operand));
                    }
            };

            class OpXArcCos : public Interpreter::Opcode0
            {
                public:
                    virtual void execute (Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Float operand = runtime[0].mFloat;
                        runtime.pop();

                        runtime.push(std::acos(operand));
                    }
            };

            class OpXArcTan : public Interpreter::Opcode0
            {
                public:
                    virtual void execute (Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Float operand = runtime[0].mFloat;
                        runtime.pop();

                        runtime.push(std::atan(operand));
                    }
            };

            class OpXArcTan2 : public Interpreter::Opcode0
            {
                public:
                    virtual void execute (Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Float y = runtime[0].mFloat;
                        runtime.pop();
                        Interpreter::Type_Float x = runtime[0].mFloat;
                        runtime.pop();

                        runtime.push(std::atan2(y, x));
                    }
            };

            class OpXHypot : public Interpreter::Opcode0
            {
                public:
                    virtual void execute (Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Float x = runtime[0].mFloat;
                        runtime.pop();
                        Interpreter::Type_Float y = runtime[0].mFloat;
                        runtime.pop();

                        runtime.push(std::sqrt(x*x + y*y));
                    }
            };

            class OpXDegRad : public Interpreter::Opcode0
            {
                public:
                    virtual void execute(Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Float deg = runtime[0].mFloat;
                        runtime.pop();

                        runtime.push((float)(deg * PI / 180.0));
                    }
            };

            class OpXRadDeg : public Interpreter::Opcode0
            {
                public:
                    virtual void execute(Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Float rad = runtime[0].mFloat;
                        runtime.pop();

                        runtime.push((float)(rad * 180.0 / PI));
                    }
            };

            class OpXPow : public Interpreter::Opcode0
            {
                public:
                    virtual void execute(Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Float base = runtime[0].mFloat;
                        runtime.pop();
                        Interpreter::Type_Float exp = runtime[0].mFloat;
                        runtime.pop();

                        runtime.push(std::pow(base, exp));
                    }
            };

            class OpXRandomLong : public Interpreter::Opcode0
            {
                public:
                    virtual void execute(Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Integer min = runtime[0].mInteger;
                        runtime.pop();
                        Interpreter::Type_Integer max = runtime[0].mInteger;
                        runtime.pop();

                        runtime.push(Misc::Rng::rollDice(max - min + 1) + min);
                    }
            };

            class OpXRandomFloat : public Interpreter::Opcode0
            {
                public:
                    virtual void execute(Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Float min = runtime[0].mFloat;
                        runtime.pop();
                        Interpreter::Type_Float max = runtime[0].mFloat;
                        runtime.pop();

                        runtime.push(Misc::Rng::rollClosedProbability() * (max - min) + min);
                    }
            };

            class OpXBitAnd : public Interpreter::Opcode0
            {
                public:
                    virtual void execute(Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Integer arg1 = runtime[0].mInteger;
                        runtime.pop();
                        Interpreter::Type_Integer arg2 = runtime[0].mInteger;
                        runtime.pop();

                        runtime.push(arg1 & arg2);
                    }
            };

            class OpXBitOr : public Interpreter::Opcode0
            {
                public:
                    virtual void execute(Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Integer arg1 = runtime[0].mInteger;
                        runtime.pop();
                        Interpreter::Type_Integer arg2 = runtime[0].mInteger;
                        runtime.pop();

                        runtime.push(arg1 | arg2);
                    }

            };

            class OpXBitXor : public Interpreter::Opcode0
            {
                public:
                    virtual void execute(Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Integer arg1 = runtime[0].mInteger;
                        runtime.pop();
                        Interpreter::Type_Integer arg2 = runtime[0].mInteger;
                        runtime.pop();

                        runtime.push(arg1 ^ arg2);
                    }
            };

            class OpXBitNot : public Interpreter::Opcode0
            {
                public:
                    virtual void execute(Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Integer arg = runtime[0].mInteger;
                        runtime.pop();

                        runtime.push(~arg);
                    }
            };

            class OpXShift : public Interpreter::Opcode0
            {
                public:
                    virtual void execute(Interpreter::Runtime& runtime)
                    {
                        Interpreter::Type_Integer value = runtime[0].mInteger;
                        runtime.pop();
                        Interpreter::Type_Integer magnitude = runtime[0].mInteger;
                        runtime.pop();

                        if (magnitude < 0)
                            runtime.push(value >> (-magnitude));
                        else
                            runtime.push(value << magnitude);
                    }
            };



            void installOpcodes(Interpreter::Interpreter& interpreter)
            {
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXSqrt, new OpXSqrt);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXSin, new OpXSin);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXCos, new OpXCos);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXTan, new OpXTan);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXArcSin, new OpXArcSin);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXArcCos, new OpXArcCos);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXArcTan, new OpXArcTan);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXArcTan2, new OpXArcTan2);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXHypot, new OpXHypot);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXDegRad, new OpXDegRad);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXRadDeg, new OpXRadDeg);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXPow, new OpXPow);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXRandomFloat, new OpXRandomFloat);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXRandomLong, new OpXRandomLong);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXBitAnd, new OpXBitAnd);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXBitOr, new OpXBitOr);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXBitXor, new OpXBitXor);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXBitNot, new OpXBitNot);
                interpreter.installSegment5 (Compiler::MWSE::Math::opcodeXShift, new OpXShift);
            }
        }
    }
}