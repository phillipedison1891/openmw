#include "extensions.hpp"

#include <components/interpreter/interpreter.hpp>
#include <components/interpreter/installopcodes.hpp>

#include "mathextensions.hpp"
#include "logicextensions.hpp"
#include "stringextensions.hpp"
#include "referenceextensions.hpp"

namespace MWScript
{
    namespace MWSE
    {
        void installOpcodes (Interpreter::Interpreter& interpreter, bool consoleOnly)
        {
            Math::installOpcodes (interpreter);
            Logic::installOpcodes (interpreter);
            String::installOpcodes (interpreter);
            Reference::installOpcodes (interpreter);
        }
    }
}
