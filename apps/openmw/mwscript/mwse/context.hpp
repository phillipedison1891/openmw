#ifndef GAME_SCRIPT_MWSE_CONTEXT_H
#define GAME_SCRIPT_MWSE_CONTEXT_H

#include <components/interpreter/interpreter.hpp>
#include "../../mwbase/world.hpp"

namespace MWScript
{
    namespace MWSE
    {
        class MWSEContext
        {

        private:
            std::map<Interpreter::Type_Integer, std::string> stringVariables;
            Interpreter::Type_Integer nextStringIndex = -1;

            std::map<Interpreter::Type_Integer, MWWorld::Ptr> references;
            Interpreter::Type_Integer nextRefIndex = -1;

        public:
            Interpreter::Type_Integer addStringVariable(std::string str);
            std::string &getStringVariable(Interpreter::Type_Integer index);

            Interpreter::Type_Integer addReference(MWWorld::Ptr ptr);
            MWWorld::Ptr getReference(Interpreter::Type_Integer index);
        };
    }
}

#endif
