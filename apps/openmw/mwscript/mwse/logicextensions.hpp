#ifndef GAME_SCRIPT_MWSE_LOGICEXTENSIONS_H
#define GAME_SCRIPT_MWSE_LOGICEXTENSIONS_H

namespace Compiler
{
    class Extensions;
}

namespace Interpreter
{
    class Interpreter;
}

namespace MWScript
{
    namespace MWSE
    {
        namespace Logic
        {
            void installOpcodes(Interpreter::Interpreter& interpreter);
        }
    }
}


#endif