#include "ref.hpp"

#include <components/interpreter/runtime.hpp>

#include "../mwbase/environment.hpp"
#include "../mwbase/world.hpp"

#include "interpretercontext.hpp"
#include "mwse/context.hpp"

MWWorld::Ptr MWScript::ExplicitRef::operator() (Interpreter::Runtime& runtime, bool required,
    bool activeOnly) const
{
    Interpreter::Type_Integer index = runtime[0].mInteger;
    runtime.pop();
    if (index < 0)
        return (static_cast<MWScript::InterpreterContext *>(&(runtime.getContext())))->getMWSEContext().getReference(index);
    else
    {
        std::string id = runtime.getStringLiteral(index);
        if (required)
            return MWBase::Environment::get().getWorld()->getPtr(id, activeOnly);
        else
            return MWBase::Environment::get().getWorld()->searchPtr(id, activeOnly);
    }
}

MWWorld::Ptr MWScript::ImplicitRef::operator() (Interpreter::Runtime& runtime, bool required,
    bool activeOnly) const
{
    MWScript::InterpreterContext& context
    = static_cast<MWScript::InterpreterContext&> (runtime.getContext());

    return context.getReference(required);
}
