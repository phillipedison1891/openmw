#include "../extensions.hpp"
#include "extensions.hpp"
#include "opcodes.hpp"
#include "../opcodes.hpp"

namespace Compiler
{
    namespace MWSE
    {
        
        void registerExtensions (Extensions& extensions, bool consoleOnly)
        {
            Actor::registerExtensions(extensions);
            Array::registerExtensions(extensions);
            File::registerExtensions(extensions);
            Iteration::registerExtensions(extensions);
            Logic::registerExtensions(extensions);
            Lua::registerExtensions(extensions);
            Magic::registerExtensions(extensions);
            Math::registerExtensions(extensions);
            Player::registerExtensions(extensions);
            String::registerExtensions(extensions);
            Reference::registerExtensions(extensions);
        }
        
        namespace Actor
        {
            void registerExtensions (Extensions& extensions)
            {}
        }
        
        namespace Array
        {
            void registerExtensions (Extensions& extensions)
            {}
        }
        
        namespace File
        {
            void registerExtensions (Extensions& extensions)
            {}
        }
        
        namespace Iteration
        {
            void registerExtensions (Extensions& extensions)
            {}
        }
        
        namespace Logic
        {
            void registerExtensions (Extensions& extensions)
            {
                extensions.registerFunction("xand", 'l', "ll", opcodeXAnd);
                extensions.registerFunction("xor", 'l', "ll", opcodeXOr);
                extensions.registerFunction("xxor", 'l', "ll", opcodeXXor);
                extensions.registerFunction("xnot", 'l', "l", opcodeXNot);
            }
        }
        
        namespace Lua
        {
            void registerExtensions (Extensions& extensions)
            {}
        }
        
        namespace Magic
        {
            void registerExtensions (Extensions& extensions)
            {}
        }
        
        namespace Math
        {
            void registerExtensions (Extensions& extensions)
            {
                extensions.registerFunction("xsqrt", 'f', "f", opcodeXSqrt);
                extensions.registerFunction("xsin", 'f', "f", opcodeXSin);
                extensions.registerFunction("xcos", 'f', "f", opcodeXCos);
                extensions.registerFunction("xtan", 'f', "f", opcodeXTan);
                extensions.registerFunction("xarcsin", 'f', "f", opcodeXArcSin);
                extensions.registerFunction("xarccos", 'f', "f", opcodeXArcCos);
                extensions.registerFunction("xarctan", 'f', "f", opcodeXArcTan);
                extensions.registerFunction("xarctan2", 'f', "ff", opcodeXArcTan2);
                extensions.registerFunction("xhypot", 'f', "ff", opcodeXHypot);
                extensions.registerFunction("xdegrad", 'f', "f", opcodeXDegRad);
                extensions.registerFunction("xraddeg", 'f', "f", opcodeXRadDeg);
                extensions.registerFunction("xpow", 'f', "ff", opcodeXPow);
                extensions.registerFunction("xrandomfloat", 'f', "ff", opcodeXRandomFloat);
                extensions.registerFunction("xrandomlong", 'l', "ll", opcodeXRandomLong);
                extensions.registerFunction("xbitand", 'l', "ll", opcodeXBitAnd);
                extensions.registerFunction("xbitor", 'l', "ll", opcodeXBitOr);
                extensions.registerFunction("xbitxor", 'l', "ll", opcodeXBitXor);
                extensions.registerFunction("xbitnot", 'l', "l", opcodeXBitNot);
                extensions.registerFunction("xshift", 'l', "ll", opcodeXShift);
            }
        }
        
        namespace Player
        {
            void registerExtensions (Extensions& extensions)
            {}
        }

        namespace String
        {
            void registerExtensions(Extensions& extensions)
            {
                extensions.registerFunction("xstringbuild", 'l', "v/vvvvvvvvvvvv", opcodeXStringBuild);
                extensions.registerFunction("xstringcompare", 'l', "vv", opcodeXStringCompare);
                extensions.registerFunction("xstringlength", 'l', "v", opcodeXStringLength);
            }
        }

        namespace Reference
        {
            void registerExtensions(Extensions& extensions)
            {
                /*extensions.registerInstruction("xadditem", "vl", opcodeXAddItem, opcodeXAddItemExplicit);
                extensions.registerInstruction("xremoveitem", "vl", opcodeXRemoveItem, opcodeXRemoveItemExplicit);*/
                extensions.registerFunction("xgetname", 'l', "", opcodeXGetName, opcodeXGetNameExplicit);
                extensions.registerFunction("xgetbaseid", 'l', "", opcodeXGetBaseId, opcodeXGetBaseIdExplicit);
                extensions.registerFunction("xgetcharge", 'f', "", opcodeXGetCharge, opcodeXGetChargeExplicit);
                extensions.registerFunction("xgetcondition", 'f', "", opcodeXGetCondition, opcodeXGetConditionExplicit);
                extensions.registerFunction("xgetencumb", 'f', "", opcodeXGetEncumb, opcodeXGetEncumbExplicit);
                extensions.registerFunction("xgetlocklevel", 'l', "", opcodeXGetLockLevel, opcodeXGetLockLevelExplicit);
                extensions.registerFunction("xgetmaxcharge", 'f', "", opcodeXGetMaxCharge, opcodeXGetMaxChargeExplicit);
                extensions.registerFunction("xgetmaxcondition", 'f', "", opcodeXGetMaxCondition, opcodeXGetMaxConditionExplicit);
                extensions.registerFunction("xgetquality", 'f', "", opcodeXGetQuality, opcodeXGetQualityExplicit);
                extensions.registerFunction("xgetref", 'l', "/v", opcodeXGetRef, opcodeXGetRefExplicit);
                extensions.registerFunction("xgetstacksize", 'l', "", opcodeXGetStackSize, opcodeXGetStackSizeExplicit);
                extensions.registerFunction("xgetvalue", 'l', "", opcodeXGetValue, opcodeXGetValueExplicit);
                extensions.registerFunction("xgetweight", 'f', "", opcodeXGetWeight, opcodeXGetWeightExplicit);
                extensions.registerFunction("xmycellid", 'l', "", opcodeXMyCellId, opcodeXMyCellIdExplicit);
                extensions.registerFunction("xmyref", 'l', "", opcodeXMyRef, opcodeXMyRefExplicit);
                extensions.registerFunction("xreftype", 'l', "", opcodeXRefType, opcodeXRefTypeExplicit);

                // The following is just an alias for the vanilla instruction, since OpenMW already handles numeric variable arguments
                extensions.registerInstruction("xposition", "ffffX", Transformation::opcodePosition, Transformation::opcodePositionExplicit);
            }
        }
    }
}