#ifndef COMPILER_MWSE_OPCODES_H
#define COMPILER_MWSE_OPCODES_H

namespace Compiler
{
    namespace MWSE
    {
        namespace Actor
        {
            const int opcodeXActivate = 0x2000401;
            const int opcodeXActivateExplicit = 0x2000402;
            const int opcodeXAddSpell = 0x2000403;
            const int opcodeXAddSpellExplicit = 0x2000404;
            const int opcodeXAiTravel = 0x2000405;
            const int opcodeXAiTravelExplicit = 0x2000406;
            const int opcodeXDrop = 0x2000407;
            const int opcodeXDropExplicit = 0x2000408;
            const int opcodeXEquip = 0x2000409;
            const int opcodeXEquipExplicit = 0x200040A;
            const int opcodeXGetAttribute = 0x200040B;
            const int opcodeXGetAttributeExplicit = 0x200040C;
            const int opcodeXGetBaseAttribute = 0x200040D;
            const int opcodeXGetBaseAttributeExplicit = 0x200040E;
            const int opcodeXGetBaseGold = 0x200040F;
            const int opcodeXGetBaseGoldExplicit = 0x2000410;
            const int opcodeXGetBaseSkill = 0x2000411;
            const int opcodeXGetBaseSkillExplicit = 0x2000412;
            const int opcodeXGetClass = 0x2000413;
            const int opcodeXGetClassExplicit = 0x2000414;
            const int opcodeXGetCombat = 0x2000415;
            const int opcodeXGetCombatExplicit = 0x2000416;
            const int opcodeXGetGold = 0x2000417;
            const int opcodeXGetGoldExplicit = 0x2000418;
            const int opcodeXGetMagic = 0x2000418;
            // const int opcodeXGetMagicExplicit = 0x2000419;
            const int opcodeXGetMaxFatigue = 0x200041A;
            const int opcodeXGetMaxFatigueExplicit = 0x200041B;
            const int opcodeXGetMaxHealth = 0x200041C;
            const int opcodeXGetMaxHealthExplicit = 0x200041D;
            const int opcodeXGetMaxMagicka = 0x200041E;
            const int opcodeXGetMaxMagickaExplicit = 0x200041F;
            const int opcodeXGetRace = 0x2000420;
            const int opcodeXGetRaceExplicit = 0x2000421;
            const int opcodeXGetService = 0x2000422;
            const int opcodeXGetServiceExplicit = 0x2000423;
            const int opcodeXGetSkill = 0x2000424;
            const int opcodeXGetSkillExplicit = 0x2000425;
            const int opcodeXGetSpell = 0x2000426;
            const int opcodeXGetSpellExplicit = 0x2000427;
            const int opcodeXGetSpellEffects = 0x2000428;
            const int opcodeXGetSpellEffectsExplicit = 0x2000429;
            const int opcodeXHasItemEquipped = 0x200042A;
            const int opcodeXHasItemEquippedExplicit = 0x200042B;
            const int opcodeXIsFemale = 0x200042C;
            const int opcodeXIsFemaleExplicit = 0x200042D;
            const int opcodeXModAttribute = 0x200042E;
            const int opcodeXModAttributeExplicit = 0x200042F;
            const int opcodeXModService = 0x2000430;
            const int opcodeXModServiceExplicit = 0x2000431;
            const int opcodeXRemoveSpell = 0x2000432;
            const int opcodeXRemoveSpellExplicit = 0x2000433;
            const int opcodeXSetBaseGold = 0x2000434;
            const int opcodeXSetBaseGoldExplicit = 0x2000435;
            const int opcodeXSetGold = 0x2000436;
            const int opcodeXSetGoldExplicit = 0x2000437;
            const int opcodeXSetLevel = 0x2000438;
            const int opcodeXSetLevelExplicit = 0x2000439;
            const int opcodeXSetService = 0x200043A;
            const int opcodeXSetServiceExplicit = 0x200043B;
            const int opcodeXStartCombat = 0x200043C;
            const int opcodeXStartCombatExplicit = 0x200043D;
        }
        
        namespace Array
        {
            const int opcodeXClearArray = 0x200043E;
            const int opcodeXCreateArray = 0x200043F;
            const int opcodeXGetArraySize = 0x2000440;
            const int opcodeXGetArrayValue = 0x2000441;
            const int opcodeXSetArrayValue = 0x2000442;
        }
        
        namespace File
        {
            const int opcodeXFileReadFloat = 0x2000443;
            const int opcodeXFileReadLong = 0x2000444;
            const int opcodeXFileReadShort = 0x2000445;
            const int opcodeXFileReadString = 0x2000446;
            const int opcodeXFileReadText = 0x2000447;
            const int opcodeXFileRewind = 0x2000448;
            const int opcodeXFileSeek = 0x2000449;
            const int opcodeXFileWriteFloat = 0x200044A;
            const int opcodeXFileWriteLong = 0x200044B;
            const int opcodeXFileWriteShort = 0x200044C;
            const int opcodeXFileWriteString = 0x200044D;
            const int opcodeXFileWriteText = 0x200044E;
        }
        
        namespace Iteration
        {
            const int opcodeXContentList = 0x200044F;
            const int opcodeXContentListExplicit = 0x2000450;
            const int opcodeXContentListFiltered = 0x2000451;
            const int opcodeXContentListFilteredExplicit = 0x2000452;
            const int opcodeXEquipmentList = 0x2000453;
            const int opcodeXEquipmentListExplicit = 0x2000454;
            const int opcodeXFirstItem = 0x2000455;
            const int opcodeXFirstNPC = 0x2000456;
            const int opcodeXFirstStatic = 0x2000457;
            const int opcodeXInventory = 0x2000458;
            const int opcodeXInventoryExplicit = 0x2000459;
            const int opcodeXNextRef = 0x200045A;
            const int opcodeXNextStack = 0x200045B;
            const int opcodeXNextStackExplicit = 0x200045C;
            const int opcodeXSpellList = 0x200045D;
            const int opcodeXSpellListExplicit = 0x200045E;
        }
        
        namespace Logic
        {
            const int opcodeXAnd = 0x200045F;
            const int opcodeXNot = 0x2000460;
            const int opcodeXOr = 0x2000461;
            const int opcodeXXor = 0x2000462;
        }
        
        namespace Lua
        {
            const int opcodeXLuaRunScript = 0x2000463;
        }
        
        namespace Magic
        {
            const int opcodeXAddEffect = 0x2000464;
            const int opcodeXCreateSpell = 0x2000465;
            const int opcodeXDeleteEffect = 0x2000466;
            const int opcodeXDeleteSpell = 0x2000467;
            const int opcodeXGetAlchemyInfo = 0x2000468;
            const int opcodeXGetBaseEffectInfo = 0x2000469;
            const int opcodeXGetEffectInfo = 0x200046A;
            const int opcodeXGetEnchantInfo = 0x200046B;
            const int opcodeXGetIngredientEffect = 0x200046C;
            const int opcodeXGetSpellInfo = 0x200046D;
            const int opcodeXSetBaseEffectInfo = 0x200046E;
            const int opcodeXSetEffectInfo = 0x200046F;
            const int opcodeXSetEnchantInfo = 0x2000470;
            const int opcodeXSetIngredientEffect = 0x2000471;
            const int opcodeXSetSpellInfo = 0x2000472;
        }
        
        namespace Math
        {
            const int opcodeXArcCos = 0x2000473;
            const int opcodeXArcSin = 0x2000474;
            const int opcodeXArcTan = 0x2000475;
            const int opcodeXArcTan2 = 0x2000476;
            const int opcodeXBitAnd = 0x2000478;
            const int opcodeXBitOr = 0x2000479;
            const int opcodeXBitXor = 0x200047A;
            const int opcodeXCos = 0x200047B;
            const int opcodeXDegRad = 0x200047C;
            const int opcodeXFloatsToLong = 0x200047D;
            const int opcodeXHypot = 0x200047E;
            const int opcodeXLongToFloats = 0x200047F;
            const int opcodeXPow = 0x2000480;
            const int opcodeXRadDeg = 0x2000481;
            const int opcodeXRandomFloat = 0x2000482;
            const int opcodeXRandomLong = 0x2000483;
            const int opcodeXShift = 0x2000484;
            const int opcodeXSin = 0x2000485;
            const int opcodeXSqrt = 0x2000486;
            const int opcodeXTan = 0x2000487;
            const int opcodeXBitNot = 0x2000488;
        }

        namespace Player
        {
            const int opcodeXGetPCTarget = 0x2000489;
            const int opcodeXGetProgressLevel = 0x200048A;
            const int opcodeXGetProgressSkill = 0x200048B;
            const int opcodeXGetSkillInfo = 0x200048C;
            const int opcodeXModProgressLevel = 0x200048D;
            const int opcodeXModProgressSkill = 0x200048E;
            const int opcodeXPCCellID = 0x200048F;
            const int opcodeXPlace = 0x2000490;
            const int opcodeXSetProgressLevel = 0x2000491;
            const int opcodeXSetProgressSkill = 0x2000492;
            const int opcodeXSetSkillInfo = 0x2000493;
        }

        namespace Reference
        {
            const int opcodeXAddItem = 0x2000494;
            const int opcodeXAddItemExplicit = 0x2000495;
            const int opcodeXDistance = 0x2000496;
            const int opcodeXDistanceExplicit = 0x2000497;
            const int opcodeXExplodeSpell = 0x2000498;
            const int opcodeXExplodeSpellExplicit = 0x2000499;
            const int opcodeXGetBaseId = 0x200049A;
            const int opcodeXGetBaseIdExplicit = 0x200049B;
            const int opcodeXGetCharge = 0x200049C;
            const int opcodeXGetChargeExplicit = 0x200049D;
            const int opcodeXGetCondition = 0x200049E;
            const int opcodeXGetConditionExplicit = 0x200049F;
            const int opcodeXGetEnchant = 0x20004A0;
            const int opcodeXGetEnchantExplicit = 0x20004A1;
            const int opcodeXGetEncumb = 0x20004A2;
            const int opcodeXGetEncumbExplicit = 0x20004A3;
            const int opcodeXGetItemCount = 0x20004A4;
            const int opcodeXGetItemCountExplicit = 0x20004A5;
            const int opcodeXGetLockLevel = 0x20004A6;
            const int opcodeXGetLockLevelExplicit = 0x20004A7;
            const int opcodeXGetMaxCharge = 0x20004A8;
            const int opcodeXGetMaxChargeExplicit = 0x20004A9;
            const int opcodeXGetMaxCondition = 0x20004AA;
            const int opcodeXGetMaxConditionExplicit = 0x20004AB;
            const int opcodeXGetModel = 0x20004AC;
            const int opcodeXGetModelExplicit = 0x20004AD;
            const int opcodeXGetName = 0x20004AE;
            const int opcodeXGetNameExplicit = 0x20004AF;
            const int opcodeXGetOwnerInfo = 0x20004B0;
            const int opcodeXGetOwnerInfoExplicit = 0x20004B1;
            const int opcodeXGetQuality = 0x20004B2;
            const int opcodeXGetQualityExplicit = 0x20004B3;
            //const int opcodeXGetRef = 0x20004B4;
            //const int opcodeXGetRefExplicit = 0x20004B5;
            const int opcodeXGetStackSize = 0x20004B6;
            const int opcodeXGetStackSizeExplicit = 0x20004B7;
            const int opcodeXGetTrap = 0x20004B8;
            const int opcodeXGetTrapExplicit = 0x20004B9;
            const int opcodeXGetValue = 0x20004BA;
            const int opcodeXGetValueExplicit = 0x20004BB;
            const int opcodeXGetWeight = 0x20004BC;
            const int opcodeXGetWeightExplicit = 0x20004BD;
            const int opcodeXMyCellId = 0x20004BE;
            const int opcodeXMyCellIdExplicit = 0x20004BF;
            const int opcodeXMyRef = 0x20004C0;
            const int opcodeXMyRefExplicit = 0x20004C1;
            // xPosition - register to vanilla Position function
            const int opcodeXPositionCell = 0x20004C2;
            const int opcodeXPositionCellExplicit = 0x20004C3;
            const int opcodeXRefID = 0x20004C4;
            const int opcodeXRefIDExplicit = 0x20004C5;
            const int opcodeXRefType = 0x20004C6;
            const int opcodeXRefTypeExplicit = 0x20004C7;
            const int opcodeXRemoveItem = 0x20004C8;
            const int opcodeXRemoveItemExplicit = 0x20004C9;
            const int opcodeXSetCharge = 0x20004CA;
            const int opcodeXSetChargeExplicit = 0x20004CB;
            const int opcodeXSetCondition = 0x20004CC;
            const int opcodeXSetConditionExplicit = 0x20004CD;
            const int opcodeXSetMaxCharge = 0x20004CE;
            const int opcodeXSetMaxChargeExplicit = 0x20004CF;
            const int opcodeXSetMaxCondition = 0x20004D0;
            const int opcodeXSetMaxConditionExplicit = 0x20004D1;
            const int opcodeXSetName = 0x20004D2;
            const int opcodeXSetNameExplicit = 0x20004D3;
            const int opcodeXSetQuality = 0x20004D4;
            const int opcodeXSetQualityExplicit = 0x20004D5;
            const int opcodeXSetRef = 0x20004D6;
            const int opcodeXSetRefExplicit = 0x20004D7;
            const int opcodeXSetTrap = 0x20004D8;
            const int opcodeXSetTrapExplicit = 0x20004D9;
            const int opcodeXSetValue = 0x20004DA;
            const int opcodeXSetValueExplicit = 0x20004DB;
            const int opcodeXSetWeight = 0x20004DC;
            const int opcodeXSetWeightExplicit = 0x20004DD;

            // Segment 3 opcodes
            const int opcodeXGetRef = 0x20041;
            const int opcodeXGetRefExplicit = 0x20042;
        }

        namespace String
        {
            //const int opcodeXStringBuild = 0x20004DE;
            const int opcodeXStringCapture = 0x20004DF;
            const int opcodeXStringCompare = 0x20004E0;
            const int opcodeXStringLength = 0x20004E1;
            const int opcodeXStringMatch = 0x20004E2;
            const int opcodeXStringParse = 0x20004E3;

            // Segment 3 opcodes
            const int opcodeXStringBuild = 0x20040;
        }

        namespace Utility
        {
            const int opcodeXGetGlobal = 0x20004E4;
            const int opcodeXGetGSString = 0x20004E5;
            const int opcodeXGetKeyBind = 0x20004E6;
            const int opcodeXGetMCPFeatureState = 0x20004E7;
            const int opcodeXGetKeyPressed = 0x20004E8;
            const int opcodeXLogMessage = 0x20004E9;
            const int opcodeXMessageFix = 0x20004EA;
            const int opcodeXScriptRunning = 0x20004EB;
            const int opcodeXSetGlobal = 0x20004EC;
            const int opcodeXSetGSString = 0x20004ED;
            const int opcodeXStartScript = 0x20004EE;
            const int opcodeXStopScript = 0x20004EF;
            const int opcodeXTextInput = 0x20004F0;
            const int opcodeXTextInputAlt = 0x20004F1;
        }
        
    }
}

#endif