#ifndef COMPILER_MWSE_EXTENSIONS_H
#define COMPILER_MWSE_EXTENSIONS_H

namespace Compiler
{
    class Extensions;
    
    namespace MWSE
    {
        void registerExtensions (Extensions& extensions, bool consoleOnly = false);
        
        namespace Actor
        {
            void registerExtensions (Extensions& extensions);
        }

        namespace Array
        {
            void registerExtensions (Extensions& extensions);
        }

        namespace File
        {
            void registerExtensions (Extensions& extensions);
        }

        namespace Iteration
        {
            void registerExtensions (Extensions& extensions);
        }

        namespace Logic
        {
            void registerExtensions (Extensions& extensions);
        }

        namespace Lua
        {
            void registerExtensions (Extensions& extensions);
        }

        namespace Magic
        {
            void registerExtensions (Extensions& extensions);
        }

        namespace Math
        {
            void registerExtensions (Extensions& extensions);
        }

        namespace Player
        {
            void registerExtensions (Extensions& extensions);
        }

        namespace String
        {
            void registerExtensions (Extensions& extensions);
        }

        namespace Reference
        {
            void registerExtensions (Extensions& extensions);
        }
    }
}
    




#endif